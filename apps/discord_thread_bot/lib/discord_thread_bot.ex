defmodule DiscordThreadBot do
  use Nostrum.Consumer

  alias Nostrum.Api
  alias DiscordThreadBot.Command
  alias DiscordThreadBot.ErrorHandler
  alias DiscordThreadApp.{Contents, Accounts}
  alias DiscordThreadApp.Contents.{Thread, Post}
  alias DiscordThreadApp.Accounts.User

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:MESSAGE_REACTION_ADD, { react }, _ws_state}) do
    message = Api.get_channel_message!(react.channel_id, react.message_id)
    embed = List.first(message.embeds)
    react_user = Api.get_user!(react.user_id)

    if String.contains?(embed.footer.text, react_user.username) do
      if to_charlist(react.emoji.name) == [128465] do
        Api.delete_message(message)
      end
    end  
  end

  def handle_event({:MESSAGE_CREATE, {message}, _ws_state}) do
    if message.channel_id == 536380112468180993 do
      Command.handle(message)
    end
  end

  def handle_event(_event) do
    :noop
  end
end
