defmodule DiscordThreadBot.Command do

  alias Nostrum.Api
  alias DiscordThreadBot.ErrorHandler
  alias DiscordThreadBot.Help
  alias DiscordThreadApp.Contents
  alias DiscordThreadApp.{Contents, Accounts}
  alias DiscordThreadApp.Accounts.User

  import DiscordThreadBot.Utils, only: [
    create_thread: 3,
    create_list: 3,
  ]

  @command_prefix "/thread"

  defp actionable_command?(message) do
    if message.author.bot == nil do
      case Accounts.get_user_by_userid!(Integer.to_string(message.author.id)) do
        %User{} = user ->
          true
        _ ->
          ErrorHandler.send_error(message, "コマンドを使用するには、こちら[#{API_URL}]でログインしてください。")
      end
    end
  end

  def handle(message) do
    if actionable_command?(message) do 
      message.content
      |> String.trim
      |> String.split
      |> execute(message)
    end
  end

  def execute([@command_prefix], message) do
    Help.help(message)
  end

  def execute([@command_prefix, "ping"], message) do
    Api.create_message!(message.channel_id, "pong!")
  end

  def execute([@command_prefix, "help"], message) do
    Help.help(message)
  end

  def execute([@command_prefix, "list", pagenum], message) do
    case Accounts.get_user_by_userid!(Integer.to_string(message.author.id)) do
      %User{} = user ->
        create_list(message, user, pagenum)
      _ ->
        :nothing
    end
  end

  def execute([@command_prefix, "create", title, type], message) do
    if String.length(title) <= 30 do
      create_thread(message, title, type)
    else
      ErrorHandler.send_error(message, "タイトルは30文字以内に収めてください")
    end
  end

  def execute([@command_prefix, "create", _title], message) do
    ErrorHandler.send_error(message, "BuyまたはSellを指定してください")
  end

  def execute([@command_prefix, _], message), do: Help.help(message.channel_id)
  def execute(_, message), do: ErrorHandler.send_error(message, "コマンドまたは引数を指定してください")
end
