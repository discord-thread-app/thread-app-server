defmodule DiscordThreadBot.Help do

  alias Nostrum.Api

  import Nostrum.Struct.Embed

  def help(message) do
    {:ok, private} = Api.create_dm(message.author.id)
    embed =
      %Nostrum.Struct.Embed{}
      |> put_title("コマンドリスト")
      |> put_field("create <タイトル> <種類>", "スレッドを作成します")
      |> put_field("list", "自分が作ったスレッドのリストをDMに送ります")
      |> put_field("help", "このリストを送ります")

    Api.create_message!(private.id, embed: embed)
  end
end