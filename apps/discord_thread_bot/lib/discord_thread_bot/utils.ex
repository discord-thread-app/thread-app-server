defmodule DiscordThreadBot.Utils do
  alias Nostrum.Api
  alias DiscordThreadApp.Repo
  alias DiscordThreadAppWeb.ThreadView
  alias DiscordThreadApp.Contents
  alias DiscordThreadApp.Contents.Thread
  alias DiscordThreadApp.Accounts
  alias DiscordThreadBot.ErrorHandler

  import Nostrum.Struct.Embed

  def full_name(message) do
    message.author.username <> "#" <> message.author.discriminator
  end

  def create_thread(message, title, type) do
    author_id = Integer.to_string(message.author.id)

    user = Accounts.get_user_by_userid!(author_id)

    params = %{
      "title" => title,
      "type" => type,
      "user_id" => user.id}

    with {:ok, %Thread{} = thread} <- Contents.create_thread(params) do
      color = if thread.type == "buy", do: 13632027, else: 4886754

      embed =
        %Nostrum.Struct.Embed{}
        |> put_title(title)
        |> put_url(Application.get_env(:discord_thread_bot, :frontend_url) <> "/threads/#{thread.id}")
        |> put_footer(full_name(message), "https://cdn.discordapp.com/avatars/#{message.author.id}/#{message.author.avatar}")
        |> put_color(color)

      thread = Repo.preload(thread, [:user, :posts])

      message_data = %{
        thread: ThreadView.render("thread.json", %{thread: thread}),
      }

      DiscordThreadAppWeb.Endpoint.broadcast!("user:" <> author_id , "thread:new", message_data)

      case Api.create_message(message.channel_id, embed: embed) do
        {:ok, message} -> IO.inspect message
      end

      Api.delete_message(message)
    else
      {:error, %Ecto.Changeset{} = changeset} ->
        ErrorHandler.send_error(message, "typeはbuyかsellを指定してください")
    end
  end

  def create_list(message, user, pagenum) do
    thread_list = 
      Contents.list_threads(%{user_id: user.id}, %{page: pagenum})
      |> Enum.map(fn thread ->
        create_embed_field(%{name: thread.title, value: thread.type})
      end)

    embed =
      %Nostrum.Struct.Embed{
        fields: thread_list
      }
      |> put_title("マイスレッドリスト")
      |> put_url(Application.get_env(:discord_thread_bot, :frontend_url) <> "/threads")

    {userid, _} = Integer.parse(user.userid)

    with {:ok, private} <- Api.create_dm(userid) do
      Api.create_message!(private.id, embed: embed)
      Api.delete_message(message)
    end
  end

  defp create_embed_field(%{name: name, value: value}, inline? \\ false) do
    %Nostrum.Struct.Embed.Field{
      inline: inline?,
      name: name,
      value: value
    }
  end
end
