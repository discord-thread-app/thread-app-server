defmodule DiscordThreadBot.MixProject do
  use Mix.Project

  def project do
    [
      app: :discord_thread_bot,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      remix: true
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {DiscordThreadBot.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nostrum, git: "https://github.com/Kraigie/nostrum.git"},
      {:discord_thread_app, in_umbrella: true},
      {:discord_thread_app_web, in_umbrella: true}
    ]
  end
end
