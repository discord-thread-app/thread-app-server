# Since configuration is shared in umbrella projects, this file
# should only configure the :discord_thread_app application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :discord_thread_bot, DiscordThreadBot,
  frontend_url: System.get_env("FRONTEND_URL") || "http://thread.devmia.net"
