# Since configuration is shared in umbrella projects, this file
# should only configure the :discord_thread_app_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :discord_thread_app_web,
  ecto_repos: [DiscordThreadApp.Repo],
  generators: [context_app: :discord_thread_app]

# Configures the endpoint
config :discord_thread_app_web, DiscordThreadAppWeb.Endpoint,
  url: [host: "0.0.0.0"],
  secret_key_base: "46JKM9520kW9szXsWkrnwrZ+9X7tBKLHkyWWurJaf+lo6mGBXPjUIjKc5DFRtyeZ",
  render_errors: [view: DiscordThreadAppWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DiscordThreadAppWeb.PubSub, adapter: Phoenix.PubSub.PG2]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
