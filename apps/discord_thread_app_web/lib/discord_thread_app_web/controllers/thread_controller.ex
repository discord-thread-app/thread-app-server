defmodule DiscordThreadAppWeb.ThreadController do
  use DiscordThreadAppWeb, :controller

  alias DiscordThreadApp.Repo
  alias DiscordThreadApp.{Contents, Accounts}
  alias DiscordThreadApp.Contents.{Thread, Post}
  alias DiscordThreadAppWeb.{PostView, ThreadView}
  alias DiscordThreadApp.Accounts.User

  action_fallback DiscordThreadAppWeb.FallbackController

  def index(%{assigns: %{current_user: user}} = conn, params) do
    threads = Contents.list_threads(%{user_id: user.id}, params)
    render(conn, "index.json", threads: threads)
  end

  def create(%{assigns: %{current_user: user}} = conn, %{"thread" => thread_params}) do
    params = Map.put(thread_params, "userid", user.userid)

    with {:ok, %Thread{} = thread} <- Contents.create_thread(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.thread_path(conn, :show, thread))
      |> render("show.json", thread: thread)
    end
  end

  def show(conn, %{"id" => id}) do
    thread = Contents.get_thread!(id)
    IO.inspect thread

    render(conn, "show.json", thread: thread)
  end

  def update(conn, %{"id" => id, "thread" => thread_params}) do
    thread = Contents.get_thread!(id)

    with {:ok, %Thread{} = thread} <- Contents.update_thread(thread, thread_params) do
      render(conn, "show.json", thread: thread)
    end
  end

  def delete(conn, %{"id" => id}) do
    thread = Contents.get_thread!(id)

    with {:ok, %Thread{}} <- Contents.delete_thread(thread) do
      send_resp(conn, :no_content, "")
    end
  end

  def create_thread_api(conn, params) do
    user =
      case Accounts.get_by_userid!(params["userid"]) do
        %User{} = user ->
          user
        {:error, _} ->
          json(conn, %{"error" => "error"})
      end

    params = Map.put(params, "user_id", user.id)

    case Contents.create_thread(params) do
      {:ok, %Thread{} = thread} ->
        thread = Repo.preload(thread, [:user, :posts])
        message = %{
          thread: ThreadView.render("thread.json", %{thread: thread}),
          timestamp: :os.system_time(:milli_seconds)
        }

        DiscordThreadAppWeb.Endpoint.broadcast!("user:"<> user.userid, "thread:new", message)
        render(conn, "simple.json", %{thread: thread})

      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, %{"error" => "error"})
    end
  end

  def create_post_api(%{assigns: %{current_user: user}} = conn, %{
    "message" => message,
    "image_path" => image_path,
    "thread_id" => thread_id} = params) do

    params =
      params
      |> Map.put("title", message)
      |> Map.put("user_id", user.id)

    with {:ok, %Post{} = post} <- Contents.create_post(params) do
      params =
        params
        |> Map.put("post_id", post.id)
        |> Map.put("path", image_path)

      {:ok, _image} = Contents.create_image(params)

      post = Repo.preload(post, [:user, :images])

      message = %{
        post: PostView.render("post_image.json", %{post: post}),
        timestamp: :os.system_time(:milli_seconds)
      }

      DiscordThreadAppWeb.Endpoint.broadcast!("room:"<> thread_id, "message:new", message)
      json(conn, %{"status" => :ok})
    end
  end

  def close(%{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    thread = Contents.get_thread!(id)

    if thread.user_id == user.id do
      {:ok, _thread} = Contents.close_thread(thread)

      DiscordThreadAppWeb.Endpoint.broadcast!("room:"<> id, "thread:closed", %{success: true})
      json(conn, %{"status" => :ok})
    else
      json(conn, %{"status" => :error})
    end
  end

  def reopen(%{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    thread = Contents.get_thread!(id)

    if thread.user_id  == user.id do
      {:ok, _thread} = Contents.reopen_thread(thread)

      DiscordThreadAppWeb.Endpoint.broadcast!("room:"<> id, "thread:reopened", %{success: true})
      json(conn, %{"status" => :ok})
    else
      json(conn, %{"status" => :error})
    end
  end

  defp authenticate(conn, _options) do
    if get_session(conn, :current_user) do
      assign(conn, :current_user, get_session(conn, :current_user))
    else
      conn
      |> redirect(to: "/")
      |> halt()
    end
  end
end
