defmodule DiscordThreadAppWeb.AuthController do
  use DiscordThreadAppWeb, :controller

  alias DiscordThreadApp.Accounts
  alias DiscordThreadApp.Accounts.User

  def create(conn, %{"user" => user_params}) do
    params = Map.put(user_params, "userid", user_params["id"])

    with %User{} = _user = Accounts.get_or_create(%{"user" => params}) do
      json(conn, %{"status" => :ok})
    end
  end

  def fetch_token(conn, %{"user" => params}) do
    params = Map.put(params, "userid", params["id"])
    with %User{} = user  <- Accounts.get_or_create!(%{"user" => params}),
         token <- Phoenix.Token.sign(conn, "user web token", user.id)
    do
      json(conn, %{"token" => token})
    end
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(401)
    |> json(%{error: "Unauthenticated!"})
  end
end
