defmodule DiscordThreadAppWeb.PostImages do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :thumb]

  def validate({file, _}) do
    ~w(.jpg .jpeg .png) |> Enum.member?(Path.extname(file.file_name))
  end

  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 100x100^ -gravity center -extent 100x100 -format png", :png}
  end

  def filename(version, {file, scope}) do
    file_name = Path.basename(file.file_name, Path.extname(file.file_name))
    "#{scope.user_id}_#{version}_#{file_name}"
  end

  def storage_dir(_, {_, scope}) do
    IO.inspect scope
    "uploads/images/uploads/thread/#{scope.thread_id}/images"
  end

  def default_url(version, _) do
    "/images/images/default_#{version}.png"
  end
end
