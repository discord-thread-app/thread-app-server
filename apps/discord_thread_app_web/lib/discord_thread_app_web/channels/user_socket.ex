defmodule DiscordThreadAppWeb.UserSocket do
  use Phoenix.Socket

  alias DiscordThreadApp.Accounts

  channel "room:*", DiscordThreadAppWeb.RoomChannel
  channel "user:*", DiscordThreadAppWeb.UserChannel

  def connect(%{"token" => token}, socket, _connect_info) do
    # max_age: 1209600 is equivalent to two weeks in seconds
    case Phoenix.Token.verify(socket, "user web token", token, max_age: 1209600) do
      {:ok, user_id} ->
        user = Accounts.get_user!(user_id)
        {:ok, assign(socket, :user, user)}
      {:error, _reason} ->
        :error
    end
  end

  def id(socket), do: "users_socket:#{socket.assigns.user.id}"
end
