defmodule DiscordThreadAppWeb.RoomChannel do
  use DiscordThreadAppWeb, :channel

  alias DiscordThreadApp.Contents
  alias DiscordThreadApp.Contents.Post
  alias DiscordThreadAppWeb.{PostView, ThreadView}

  import Ecto.Query, warn: false
  alias DiscordThreadApp.Repo

  def join("room:" <> thread_id, _payload, socket) do
    if authorized?(socket) do
      case Contents.get_thread!(thread_id) do
        nil ->
          {:error, %{reason: "unauthorized"}}
        thread ->
          {:ok, assign(socket, :thread_id, thread.id)}
      end
    else
        {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_in("message:new", message, socket) do
    case Contents.create_post(%{
      "title" => message["message"],
      "thread_id" => socket.assigns.thread_id,
      "user_id" => socket.assigns.user.id}) do

      {:ok, %Post{} = post} ->
        post = Repo.preload(post, [:images, :user])

        broadcast! socket, "message:new", %{
          post: PostView.render("post.json", %{post: post}),
          timestamp: :os.system_time(:milli_seconds)
        }

        {:noreply, socket}
      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, socket}
    end
  end


  # Add authorization logic here as required.
  defp authorized?(socket) do
    unless socket.assigns.user do
      false
    end

    true
  end
end
