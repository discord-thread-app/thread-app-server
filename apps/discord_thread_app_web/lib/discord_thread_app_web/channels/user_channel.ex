defmodule DiscordThreadAppWeb.UserChannel do
  use DiscordThreadAppWeb, :channel
  alias DiscordThreadAppWeb.ThreadView

  def join("user:" <> userid, payload, socket) do
    if authorized?(socket, userid) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (user:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  def handle_in("thread:new", thread, socket) do
    broadcast! "user:#{socket.assigns.userid}", "thread:new", %{
      thread: ThreadView.render("thread.json", %{thread: thread}),
      timestamp: :os.system_time(:milli_seconds)
    }

    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(socket, _userid) do
    true
  end
end
