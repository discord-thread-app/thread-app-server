defmodule DiscordThreadAppWeb.ImageView do
  use DiscordThreadAppWeb, :view
  alias DiscordThreadAppWeb.ImageView

  def render("index.json", %{images: images}) do
    %{data: render_many(images, ImageView, "image.json")}
  end

  def render("show.json", %{image: image}) do
    %{data: render_one(image, ImageView, "image.json")}
  end

  def render("image.json", %{image: image}) do
    %{id: image.id,
      path: DiscordThreadApp.PostImage.url({image.path, image}, :original),
      uuid: image.uuid}
  end
end
