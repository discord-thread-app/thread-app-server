defmodule DiscordThreadAppWeb.PostView do
  use DiscordThreadAppWeb, :view
  alias DiscordThreadAppWeb.{UserView, ImageView, PostView}

  def render("index.json", %{posts: posts}) do
    %{data: render_many(posts, PostView, "post.json")}
  end

  def render("show.json", %{post: post}) do
    %{data: render_one(post, PostView, "post.json")}
  end

  def render("post.json", %{post: post}) do
    %{id: post.id,
      title: post.title,
      user: render_one(post.user, UserView, "user.json"),
      image: render_many(post.images, ImageView, "image.json")}
  end

  def render("post_image.json", %{post: post}) do
    %{id: post.id,
      title: post.title,
      user: render_one(post.user, UserView, "user.json"),
      image: render_many(post.images, ImageView, "image.json")
    }
  end
end
