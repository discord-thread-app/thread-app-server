defmodule DiscordThreadAppWeb.UserView do
  use DiscordThreadAppWeb, :view
  alias DiscordThreadAppWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      avatar: user.avatar,
      userid: user.userid,
      username: user.username,
      discriminator: user.discriminator}
  end
end
