defmodule DiscordThreadAppWeb.ThreadView do
  use DiscordThreadAppWeb, :view

  alias DiscordThreadAppWeb.{ThreadView, UserView, PostView}

  def render("index.json", %{threads: threads}) do
    %{
      data: render_many(threads.entries, ThreadView, "thread.json"),
      pagination: %{
        page_number: threads.page_number,
        page_size: threads.page_size,
        total_pages: threads.total_pages,
        total_entries: threads.total_entries
      }}
  end

  def render("show.json", %{thread: thread}) do
    %{data: render_one(thread, ThreadView, "thread.json")}
  end

  def render("thread.json", %{thread: thread}) do
    %{id: thread.id,
      title: thread.title,
      type: thread.type,
      closed: thread.closed,
      user_id: thread.user_id,
      user: render_one(thread.user, UserView, "user.json"),
      posts: render_many(thread.posts, PostView, "post.json")}
  end

  def render("simple.json", %{thread: thread}) do
    %{id: thread.id,
      title: thread.title,
      type: thread.type,
      closed: thread.closed,
      user_id: thread.user_id}
  end
end
