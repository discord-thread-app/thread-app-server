defmodule DiscordThreadAppWeb.Router do
  use DiscordThreadAppWeb, :router

  pipeline :api do
    require Logger
    use Plug.ErrorHandler

    def handle_errors(conn, %{reason: %Phoenix.Router.NoRouteError{}}) do
      conn
      |> put_status(404)
      |> json(%{message: "Not Found"})
    end

    def handle_errors(conn, _) do
      conn
      |> put_status(500)
      |> json(%{message: "Internal Server Error"})
    end

    if Mix.env == :dev do
      use Plug.Debugger, otp_app: :discord_thread_app
    end

    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug :accepts, ["json"]
    plug DiscordThreadApp.Plugs.Auth
  end


  scope "/api", DiscordThreadAppWeb do
    pipe_through :api

    post "/token", AuthController, :fetch_token
    post "/create_thread", ThreadController, :create_thread_api
  end

  scope "/api", DiscordThreadAppWeb do
    pipe_through :authenticated

    resources "/threads", ThreadController

    post "/threads/create_post", ThreadController, :create_post_api
    post "/threads/close/:id", ThreadController, :close
    post "/threads/reopen/:id", ThreadController, :reopen
  end
end
