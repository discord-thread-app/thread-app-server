defmodule DiscordThreadApp.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :access_token, :string
    field :avatar, :string
    field :discriminator, :string
    field :expires_at, :integer
    field :refresh_token, :string
    field :userid, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:access_token, :expires_at, :refresh_token, :avatar, :userid, :username, :discriminator])
    |> validate_required([:avatar, :userid, :username, :discriminator])
  end
end
