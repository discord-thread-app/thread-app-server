
defmodule DiscordThreadApp.Plugs.Auth do
  import Plug.Conn
  alias DiscordThreadApp.Accounts

  def init(options) do
    options
  end

  def call(conn, _) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization") do
      case Phoenix.Token.verify(conn, "user web token", token, max_age: 1209600) do
        {:ok, user_id} ->
          user = Accounts.get_user!(user_id)
          assign(conn, :current_user, user)
        {:error, _reason} ->
          :error
      end
    else
      _ ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(403, Jason.encode!(%{error: "Invalid credentials."}))
        |> halt
    end
  end
end
