defmodule DiscordThreadApp.Repo do
  use Ecto.Repo,
    otp_app: :discord_thread_app,
    adapter: Ecto.Adapters.Postgres
  use Scrivener, page_size: 5
end
