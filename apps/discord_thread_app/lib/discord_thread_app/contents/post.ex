defmodule DiscordThreadApp.Contents.Post do
  use Ecto.Schema
  import Ecto.Changeset

  alias DiscordThreadApp.Contents.{Thread, Image}

  schema "posts" do
    field :title, :string

    belongs_to :user, DiscordThreadApp.Accounts.User
    belongs_to :thread, Thread
    has_many :images, Image, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :thread_id, :user_id])
    |> validate_required([:title, :thread_id, :user_id])
  end
end
