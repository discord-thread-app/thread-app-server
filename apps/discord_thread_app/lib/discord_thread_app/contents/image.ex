defmodule DiscordThreadApp.Contents.Image do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  schema "images" do
    field :path, DiscordThreadApp.PostImage.Type
    field :uuid, :string
    belongs_to :thread, DiscordThreadApp.Contents.Thread
    belongs_to :post, DiscordThreadApp.Contents.Post
    belongs_to :user, DiscordThreadApp.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(image, attrs) do
    image
    |> cast(attrs, [:user_id, :post_id, :thread_id])
    |> validate_required([:user_id, :post_id, :thread_id])
    |> cast_attachments(attrs, [:path])
    |> check_uuid
  end

  defp check_uuid(changeset) do
    case get_field(changeset, :uuid) do
      nil ->
        force_change(changeset, :uuid, UUID.uuid4())
      _ ->
        changeset
    end
  end
end
