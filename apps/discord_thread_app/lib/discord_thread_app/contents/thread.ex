defmodule DiscordThreadApp.Contents.Thread do
  use Ecto.Schema
  import Ecto.Changeset


  schema "threads" do
    field :title, :string
    field :type, :string
    field :closed, :boolean, default: false

    belongs_to :user, DiscordThreadApp.Accounts.User
    has_many :posts, DiscordThreadApp.Contents.Post, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(thread, attrs) do
    thread
    |> cast(attrs, [:title, :type, :closed, :user_id])
    |> validate_required([:title, :type, :closed, :user_id])
    |> validate_inclusion(:type, ["buy", "sell"])
  end
end
