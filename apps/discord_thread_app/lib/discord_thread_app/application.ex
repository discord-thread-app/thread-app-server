defmodule DiscordThreadApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  import Supervisor.Spec

  def start(_type, _args) do
    children = [
      DiscordThreadApp.Repo,
      worker(
        Cachex,
        [
          :user_cache,
          [
            default_ttl: 25000,
            ttl_interval: 1000,
            limit: 2500
          ]
        ],
        id: :cachex_user
      ),
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: DiscordThreadApp.Supervisor)
  end
end
