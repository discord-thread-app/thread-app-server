# Since configuration is shared in umbrella projects, this file
# should only configure the :discord_thread_app application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :discord_thread_app, DiscordThreadApp.Repo,
  username: "postgres",
  password: "postgres",
  database: "discord_thread_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
