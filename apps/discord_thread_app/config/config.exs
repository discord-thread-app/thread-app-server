# Since configuration is shared in umbrella projects, this file
# should only configure the :discord_thread_app application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :discord_thread_app,
  ecto_repos: [DiscordThreadApp.Repo]

config :arc,
  storage: Arc.Storage.Local

import_config "#{Mix.env()}.exs"
