# Since configuration is shared in umbrella projects, this file
# should only configure the :discord_thread_app application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :discord_thread_app, DiscordThreadApp.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "app_dev",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool_size: 10

