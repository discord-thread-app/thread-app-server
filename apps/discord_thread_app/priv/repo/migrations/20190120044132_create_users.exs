defmodule DiscordThreadApp.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :access_token, :text
      add :expires_at, :text
      add :refresh_token, :text
      add :avatar, :text
      add :userid, :text
      add :username, :text
      add :discriminator, :text

      timestamps()
    end

  end
end
