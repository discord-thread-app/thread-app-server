defmodule DiscordThreadApp.Repo.Migrations.CreateImages do
  use Ecto.Migration

  def change do
    create table(:images) do
      add :path, :text
      add :uuid, :text
      add :thread_id, references(:threads, on_delete: :nothing)
      add :post_id, references(:posts, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:images, [:thread_id])
    create index(:images, [:post_id])
    create index(:images, [:user_id])
  end
end
