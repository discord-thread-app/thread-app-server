#!/usr/bin/env bash

docker-compose run app mix deps.get --only prod
docker-compose run app env MIX_ENV=prod mix phx.digest
docker-compose run app env MIX_ENV=prod mix compile
docker-compose run app env MIX_ENV=prod mix ecto.migrate
docker-compose run frontend yarn install

docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
